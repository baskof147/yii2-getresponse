<?php

namespace nuffic\getresponse\exceptions;

use yii\base\Exception;

/**
 * Class BlacklistedContactException
 * @package nuffic\getresponse\exceptions
 */
class BlacklistedContactException extends Exception
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Blacklisted Contact';
    }
}
