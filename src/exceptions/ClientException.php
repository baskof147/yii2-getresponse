<?php

namespace nuffic\getresponse\exceptions;

use Exception;

/**
 * Class ClientException
 * @package nuffic\getresponse\exceptions
 */
class ClientException extends Exception
{
}
